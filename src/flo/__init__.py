from dash import Dash, html, dcc, callback, Output, Input
import plotly.express as px
from obspy import UTCDateTime
from obspy.clients.fdsn import Client
import pendulum


client = Client("RESIF")
begin = pendulum.datetime(2024, 5, 29, 16, 17)
network = "FR"
station = "OGSA"
location = "00"
channel = "HN*"
days = 1
LENGTH = 120


start = UTCDateTime(begin)
# Get stream
stream = client.get_waveforms(
    network, station, location, channel, start, start + LENGTH, attach_response=True
)
stream.remove_response()
stream.filter("highpass", freq=1)

orientations = {trace.stats.channel: index for index, trace in enumerate(stream.traces)}

app = Dash()

app.layout = [
    html.H1(children='Flo footsteps', style={'textAlign':'center'}),
    dcc.Dropdown(list(orientations.keys()), 'HNE', id='dropdown-selection'),
    dcc.Graph(id='graph-content')
]

@callback(
    Output('graph-content', 'figure'),
    Input('dropdown-selection', 'value')
)
def update_graph(value):
    return px.line(stream.traces[orientations[value]].data)

if __name__ == '__main__':
    app.run(debug=False)
